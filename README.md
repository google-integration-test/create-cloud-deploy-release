# create-cloud-deploy-release

**Status: [Beta][beta]**

> - `google_cloud_support_feature_flag` (Beta) flag needs to be enabled to use the Component.

The `create-cloud-deploy-release` GitLab Component creates a Cloud Deploy release to manage the deployment of an application to one or more Google Kubernetes Engine (GKE) or Cloud Run targets.

## Prerequisites

- Set up the Google Cloud workload identity federation by following the steps in [Google Cloud workload identity federation and IAM policies][onboarding].
- The workload identity used to create the release should have proper permissions configured. Please check [Authorization](#authorization) section.
- If you don't already have a Cloud Deploy delivery pipeline, then create one for your deployment target. You can use one of the following quickstarts to create a Cloud Deploy delivery pipeline:
  - [Deploy an app to GKE using Cloud Deploy][gke]
  - [Deploy an app to Cloud Run using Cloud Deploy][cloud-run]

## Usage
Add the following to your .gitlab-ci.yml file:

``` yaml
include:
  - component: gitlab.com/google-gitlab-components/cloud-deploy/create-cloud-deploy-release@0.1.0
    inputs:
      project_id: my-project
      name: release-$CI_PIPELINE_ID
      delivery_pipeline: my-delivery-pipeline
      region: us-central1
      images: img1=path/to/img1,img2=path/to/img2
```

## Authentication

- The component authenticates to Google Cloud services using a workload identity pool. To set up your workload identity pool and provider with the default permissions, follow the steps in the [Google Cloud workload identity federation and IAM policies][onboarding] tutorial.

## Inputs

| Input | Description | Example| Default Value |
| ----- | ----------- | ------ | ------------- |
| `name` | (Required) The name for the release | `my-release` | |
| `project_id` | (Required) The ID of the Google Cloud project in which to deploy the service | `my-project` | |
| `region` | (Required) The region of the delivery pipeline | `us-central1` | |
| `delivery_pipeline` | (Required) The [delivery pipeline][cd-pipeline] to use for the release | `my-pipeline` | | 
| `source` | (Required) The location of the files to be included in the release; typically application configuration manifests | `web/` | `.`|
| `images` | (Optional) The details of the application image(s) to be released | In the format: `img1=us-docker.pkg.dev/my-project/my-repo/img1@v1,img2=us-docker.pkg.dev/my-project/my-repo/img2@v1` (no space)| |
| `description` | (Optional) The a description of the release | `my description` | |
| `to_target` |  (Optional) The specific target to deliver into upon release creation | `test-target` | |
| `skaffold_file` | (Optional) The path of the skaffold file absolute or relative to the source directory | `skaffold.yaml` | |
| `skaffold_version` | (Optional) The version of the Skaffold binary | `2.8` | |
| `initial_rollout_annotations` | (Optional) The annotations to apply to the initial rollout when creating the release | In the format: `annotation1=val1,annotation2=val2` (no space) | |
| `initial_rollout_labels` | (Optional) The labels to apply to the initial rollout when creating the release | In the format: `label1=val1,label2=val2` (no space) | |
| `initial_rollout_phase_id` | (Optional) The phase to start the initial rollout at when creating the release | `stable` | |
| `stage` | (Optional) The GitLab CI/CD stage | `my-stage` | `deploy` |
| `as` | (Optional) The name of the job to be executed | `my-job` | `create-cloud-deploy-release` |

## Authorization

### Cloud Deploy Release Creation

To use the Cloud Deploy component, grant the following roles to your workload identity pool:
- Cloud Deploy Releaser ([`roles/clouddeploy.releaser`][sa-cd-releaser])
  - To create Cloud Deploy releases and rollouts
- Cloud Storage Admin ([`roles/storage.admin`][sa-storage-admin])
  - To get control of Cloud Storage objects and buckets
- Service Account User ([`roles/iam.serviceAccountUser`][sa-sa-user])
  - To run operations as the service account

For example, to grant the `roles/clouddeploy.releaser` role to all principals in a workload identity pool matching `developer_access=true` attribute mapping by the gcloud CLI:

``` bash
# Replace ${PROJECT_ID}, ${PROJECT_NUMBER}, ${LOCATION}, ${POOL_ID} with your values below

WORKLOAD_IDENTITY=principalSet://iam.googleapis.com/projects/${PROJECT_NUMBER}/locations/global/workloadIdentityPools/${POOL_ID}/attribute.developer_access/true

gcloud projects add-iam-policy-binding ${PROJECT_ID} --member="${WORKLOAD_IDENTITY}" --role="roles/clouddeploy.releaser"
```

Learn more about [workload identity principal identifiers][principal] and [supported OIDC claims][claims] available for the GitLab on Google Cloud integration.

### Cloud Deploy Execution Service Account

By default, Cloud Deploy uses the [default Compute Engine service account][default-sa] (`[project-number]-compute@developer.gserviceaccount.com`) as the internal [execution service account][execution-sa] to render manifests and deploy to your targets. That service account has sufficient permissions by default.

For production use cases, it is recommended that you create one or more dedicated service accounts [as detailed in the Cloud Deploy documentation][what-sa-to-create].

You might find that organizational policy rules prevent certain permissions being assigned to default service accounts. In this case please consult your organization administrators.

By default the execution service account used by Cloud Deploy requires the following roles:

- Cloud Deploy Job Runner ([`roles/clouddeploy.jobRunner`][job-runner])
  - To execute Cloud Deploy work
- Cloud Logging Writer ([`roles/logging.logWriter`][sa-log-writter])
  - To write logs
- Cloud Storage Viewer ([`storage/object.viewer`][sa-obj-viewer])
  - To read Cloud Deploy artifacts
- Cloud Storage Creator ([`storage/object.creator`][sa-obj-creator])
  - To write Cloud Deploy artifacts

For example, to grant the `roles/logging.logWriter` to a dedicated service account using the gcloud CLI:

``` bash
# Replace ${PROJECT_ID} and ${SERVICE_ACCOUNT} with your values below

gcloud projects add-iam-policy-binding ${PROJECT_ID} --member=serviceAccount:${SERVICE_ACCOUNT} --role='roles/logging.logWriter'
```

The service account used by Cloud Deploy additionally needs permissions that allow deployment of a service to your target runtime (i.e. GKE or Cloud Run) as described in the following sections. This could be:

- The default compute service account, or
- A separate custom service account dedicated to deployment

### Cloud Run Authorization

To deploy to Cloud Run, the service account used by Cloud Deploy needs the following permissions:

- Cloud Run Developer ([`roles/run.developer`][sa-cr-dev])
  - To read and write access to all Cloud Run resources
- Service Account User ([`roles/iam.serviceAccountUser`][sa-sa-user])
  - To run operations as the service account

### GKE Authorization

To deploy to GKE, the service account used by Cloud Deploy needs the following permissions:

- Kubernetes Engine Developer ([`roles/container.developer`][sa-container-dev])
  - To provide access to Kubernetes API objects inside clusters
- Service Account User ([`roles/iam.serviceAccountUser`][sa-sa-user])
  - To run operations as the service account

In all cases, you should follow the principle of least privilege when assigning IAM roles. When possible, grant roles at the resource level as opposed to the project level.

[cd-pipeline]: https://cloud.google.com/deploy/docs/terminology#delivery_pipeline
[gke]: https://cloud.google.com/deploy/docs/deploy-app-gke
[cloud-run]: https://cloud.google.com/deploy/docs/deploy-app-run
[beta]: https://docs.gitlab.com/ee/policy/experiment-beta-support.html#beta
[onboarding]: https://docs.gitlab.com/ee/integration/google_cloud_iam.html
[principal]: https://cloud.google.com/iam/docs/principal-identifiers
[claims]: https://docs.gitlab.com/ee/integration/google_cloud_iam.html#oidc-custom-claims
[default-sa]: https://cloud.google.com/iam/docs/service-account-types#default
[execution-sa]: https://cloud.google.com/deploy/docs/cloud-deploy-service-account#execution_service_account
[what-sa-to-create]: https://cloud.google.com/deploy/docs/cloud-deploy-service-account#what_service_accounts_to_create
[job-runner]: https://cloud.google.com/iam/docs/understanding-roles#clouddeploy.jobRunner
[sa-sa-user]: https://cloud.google.com/iam/docs/understanding-roles#iam.serviceAccountUser
[sa-log-writter]: https://cloud.google.com/iam/docs/understanding-roles#logging.logWriter
[sa-obj-viewer]: https://cloud.google.com/iam/docs/understanding-roles#storage.objectViewer
[sa-obj-creator]: https://cloud.google.com/iam/docs/understanding-roles#storage.objectCreator
[sa-cr-dev]: https://cloud.google.com/iam/docs/understanding-roles#run.developer
[sa-container-dev]: https://cloud.google.com/iam/docs/understanding-roles#container.developer
[sa-cd-releaser]: https://cloud.google.com/iam/docs/understanding-roles#clouddeploy.releaser
[sa-storage-admin]: https://cloud.google.com/iam/docs/understanding-roles#storage.admin
[google-project]: https://cloud.google.com/resource-manager/docs/creating-managing-projects#identifying_projects
[delivery-pipeline]: https://cloud.google.com/deploy/docs/terminology#delivery_pipeline
[release]: https://cloud.google.com/deploy/docs/terminology#release
[region]: https://cloud.google.com/compute/docs/regions-zones

